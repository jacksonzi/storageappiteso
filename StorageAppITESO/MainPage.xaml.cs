﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=391641

namespace StorageAppITESO
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();

            this.NavigationCacheMode = NavigationCacheMode.Required;

            Windows.Storage.ApplicationData.Current.LocalSettings.Values["lastpage"] = "34";
            Windows.Storage.ApplicationData.Current.RoamingSettings.Values["lastpagecloud"] = "67";

        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // TODO: Prepare page for display here.

            // TODO: If your application contains multiple pages, ensure that you are
            // handling the hardware Back button by registering for the
            // Windows.Phone.UI.Input.HardwareButtons.BackPressed event.
            // If you are using the NavigationHelper provided by some templates,
            // this event is handled for you.
        }

        private async void btnStorage_Click(object sender, RoutedEventArgs e)
        {
            StorageFolder folder = ApplicationData.Current.LocalFolder;
            StorageFolder roamingfolder = ApplicationData.Current.RoamingFolder;
            StorageFile file = await folder.CreateFileAsync("myfile.txt",
                CreationCollisionOption.ReplaceExisting);

            await FileIO.WriteTextAsync(file, "my text in the file. go ITESO!!");
        }

        private async void btnRead_Click(object sender, RoutedEventArgs e)
        {
            StorageFolder folder = ApplicationData.Current.LocalFolder;
            StorageFile file = await folder.CreateFileAsync("myfile.txt",
                CreationCollisionOption.OpenIfExists);
            txtContent.Text = await FileIO.ReadTextAsync(file);
        }

        private async void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            StorageFolder folder = ApplicationData.Current.LocalFolder;
            StorageFile file = await folder.CreateFileAsync("myfile.txt",
                CreationCollisionOption.OpenIfExists);

            await FileIO.AppendTextAsync(file, txtUpdateInfo.Text);
        }

        private async void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            StorageFolder folder = ApplicationData.Current.LocalFolder;
            StorageFile file = await folder.CreateFileAsync("myfile.txt",
                CreationCollisionOption.OpenIfExists);

            await file.DeleteAsync();
        }

        private async void btnSettings_Click(object sender, RoutedEventArgs e)
        {
            string info = "";

            info = Windows.Storage.ApplicationData.Current.LocalSettings.Values["lastpage"].ToString();
            string roaminfo = Windows.Storage.ApplicationData.Current.RoamingSettings.Values["lastpagecloud"].ToString();
            info += " / " + roaminfo;

            await new MessageDialog(info).ShowAsync();
        }
    }
}
